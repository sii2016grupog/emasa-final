/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.List;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * @author GRUPO-G
 */
@Entity
@DiscriminatorValue("SUPERVISOR")
public class Supervisor extends Usuario {
    private static final long serialVersionUID = 1L;
    @OneToMany(mappedBy="supervisor_A")
    private List<Aviso> avisos;
    @OneToMany(mappedBy="supervisor_O")
    private List<Orden> orden;

     public Supervisor(int i, String n, String a, String c, String calle, String dni, String tlf) {
        super(i, n, a, c, calle, dni, tlf);
        super.setRol(Rol.SUPERVISOR); 
    }
     
     public Supervisor (){
         
     }

    public List<Aviso> getAvisos() {
        return avisos;
    }

    public List<Orden> getOrden() {
        return orden;
    }

    public void setAvisos(List<Aviso> avisos) {
        this.avisos = avisos;
    }

    public void setOrden(List<Orden> orden) {
        this.orden = orden;
    }
    
    
}
