/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * @author GRUPO-G
 */
@Entity
public class Afectado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_afectado;
    private String nombre;
    @Column (nullable = false)
    private String direccion;
    private String telefono;
    private String disponibilidad;
    
    @OneToOne
    @JoinColumn (nullable = false)
    private Aviso aviso_A;

    public Long getId_afectado() {
        return id_afectado;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getDisponibilidad() {
        return disponibilidad;
    }

    public Aviso getAviso_A() {
        return aviso_A;
    }

    public void setId_afectado(Long id_afectado) {
        this.id_afectado = id_afectado;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setDisponibilidad(String disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public void setAviso_A(Aviso aviso_A) {
        this.aviso_A = aviso_A;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_afectado != null ? id_afectado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Afectado)) {
            return false;
        }
        Afectado other = (Afectado) object;
        if ((this.id_afectado == null && other.id_afectado != null) || (this.id_afectado != null && !this.id_afectado.equals(other.id_afectado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Afectados[ id=" + id_afectado + " ]";
    }
    
}