/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 * @author GRUPO-G
 */
@Entity
public class Notificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_notificaciones;
    @Column (nullable = false)
    private String descripcion;
    @Column (nullable = false)
    private String fecha;
    private String notificador;
    private String tlf_notificador;
    private String email;
    
    @ManyToOne
    @JoinColumn (nullable = false)
    private Operador operador_N;
    
    @OneToOne
    private Aviso aviso_N;

    public Operador getOperador_N() {
        return operador_N;
    }

    public Long getId_notificaciones() {
        return id_notificaciones;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getFecha() {
        return fecha;
    }

    public String getNotificador() {
        return notificador;
    }

    public String getTlf_notificador() {
        return tlf_notificador;
    }

    public String getEmail() {
        return email;
    }

    public Aviso getAviso_N() {
        return aviso_N;
    }

    public void setId_notificaciones(Long id_notificaciones) {
        this.id_notificaciones = id_notificaciones;
    }

    public void setOperador_N(Operador operador_N) {
        this.operador_N = operador_N;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setNotificador(String notificador) {
        this.notificador = notificador;
    }

    public void setTlf_notificador(String tlf_notificador) {
        this.tlf_notificador = tlf_notificador;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAviso_N(Aviso aviso_N) {
        this.aviso_N = aviso_N;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_notificaciones != null ? id_notificaciones.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notificacion)) {
            return false;
        }
        Notificacion other = (Notificacion) object;
        if ((this.id_notificaciones == null && other.id_notificaciones != null) || (this.id_notificaciones != null && !this.id_notificaciones.equals(other.id_notificaciones))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Notificaciones[ id=" + id_notificaciones + " ]";
    }
    
}
