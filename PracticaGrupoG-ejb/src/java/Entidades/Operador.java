/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.List;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

/**
 * @author GRUPO-G
 */
@Entity
@DiscriminatorValue("OPERADOR")
public class Operador extends Usuario {
    private static final long serialVersionUID = 1L;
    @OneToMany(mappedBy="operador_N")
    private List<Notificacion> notificaciones;
    @OneToMany(mappedBy="operador_A")
    private List<Aviso> avisos;

    public Operador(int i, String n, String a, String c, String calle, String dni, String tlf) {
        super(i, n, a, c, calle, dni, tlf);
        super.setRol(Rol.OPERADOR); 
    }
    
    public Operador (){
       
    }

    public List<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public List<Aviso> getAvisos() {
        return avisos;
    }

    public void setNotificaciones(List<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }

    public void setAvisos(List<Aviso> avisos) {
        this.avisos = avisos;
    }
    
}
