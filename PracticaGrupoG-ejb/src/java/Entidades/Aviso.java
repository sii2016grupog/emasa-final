/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * @author GRUPO-G
 */
@Entity
//@NamedQueries({@NamedQuery(name = "Aviso.getNAviso", query = "SELECT av FROM Avisos AS form WHERE  av.nombre = :nombre ")})  //AND av.activo= enCola
//@NamedQueries({@NamedQuery(name = "Aviso.getLAviso", query = "SELECT av FROM Avisos AS form WHERE  av.nombre = :nombre AND av.Estado=abierto OR av.Estado=despachadoMovilidad OR av.Estado=generadaOT")})
//@NamedQueries({@NamedQuery(name = "Aviso.getCAviso", query = "SELECT av FROM Avisos AS form WHERE  av.nombre = :nombre AND av.activo= cerrado")})

public class Aviso implements Serializable {
    public enum Urgencia{Urgente,Planificada};
    public enum TiposEstado{enCola,abierto,despachadoMovilidad,generadaOT,cerrado};
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id_Aviso;
    @Column (nullable = false)
    private String Nombre;
    @Column (nullable = false)
    private Urgencia Grado_Urgencia;
    @Column (nullable = false)
    private TiposEstado Estado;
    private String Informe;
    
    @OneToOne (mappedBy="aviso_A")
    @JoinColumn (nullable = false)
    private Afectado afectado;
    
    @OneToOne (mappedBy="aviso_N")
    //@JoinColumn (nullable = false)
    private Notificacion notificaciones;
    
    @OneToMany (mappedBy="aviso_O")
    private List<Orden> ordenes;
    
    @ManyToOne
    private Supervisor supervisor_A;
    
    @ManyToOne
    @JoinColumn (nullable = false)
    private Operador operador_A;

    public Afectado getAfectados() {
        return afectado;
    }

    public Notificacion getNotificaciones() {
        return notificaciones;
    }

    public List<Orden> getOrdenes() {
        return ordenes;
    }

    public Supervisor getSupervisor_A() {
        return supervisor_A;
    }

    public Operador getOperador_A() {
        return operador_A;
    }
    
    public Long getId_Aviso() {
        return id_Aviso;
    }

    public String getNombre() {
        return Nombre;
    }
    
    public Urgencia getGrado_Urgencia() {
        return Grado_Urgencia;
    }

    public TiposEstado getEstado() {
        return Estado;
    }

    public String getInforme() {
        return Informe;
    }

    public void setId_Aviso(Long id_Aviso) {
        this.id_Aviso = id_Aviso;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public void setGrado_Urgencia(Urgencia Grado_Urgencia) {
        this.Grado_Urgencia = Grado_Urgencia;
    }

    public void setEstado(TiposEstado Estado) {
        this.Estado = Estado;
    }
    
    public void setInforme(String Informe) {
        this.Informe = Informe;
    }

    public void setAfectados(Afectado afectado) {
        this.afectado = afectado;
    }

    public void setNotificaciones(Notificacion notificaciones) {
        this.notificaciones = notificaciones;
    }

    public void setOrdenes(List<Orden> ordenes) {
        this.ordenes = ordenes;
    }

    public void setSupervisor_A(Supervisor supervisor_A) {
        this.supervisor_A = supervisor_A;
    }

    public void setOperador_A(Operador operador_A) {
        this.operador_A = operador_A;
    }
    
   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id_Aviso != null ? id_Aviso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Aviso)) {
            return false;
        }
        Aviso other = (Aviso) object;
        if ((this.id_Aviso == null && other.id_Aviso != null) || (this.id_Aviso != null && !this.id_Aviso.equals(other.id_Aviso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entidades.Avisos[ id=" + id_Aviso + " ]";
    }
    
}
