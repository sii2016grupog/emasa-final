/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Entidades.Afectado;
import Entidades.Aviso;
import Entidades.Orden.TiposEstado;
import Entidades.Notificacion;
import Entidades.Operador;
import Entidades.Orden;
import Entidades.Supervisor;
import Entidades.Trabajador;
import Entidades.Usuario;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class NegocioImpl implements Negocio {

    @PersistenceContext(unitName = "PracticaGrupoG-ejbPU")
    private EntityManager em;

    @Override
    public void crearUsuarios() {
        Query q = em.createQuery("SELECT u FROM Usuario u");
        if (q.getResultList().isEmpty()) {
            try {
                DateFormat df = new SimpleDateFormat("ddMMyyyy");

                Operador u1 = new Operador();
                u1.setId_Usuario((long) 1);
                u1.setNombre("Homer");
                u1.setApellidos("Simpsons");
                u1.setContrasenia("asdf");
                u1.setDireccion("Calle Falsa");
                Date date = df.parse(String.valueOf("28041989"));
                u1.setFecha_nacimiento(date);
                u1.setRol(Usuario.Rol.OPERADOR);
                u1.setTelefono("952111111");
                u1.setDni("111111111");

                Supervisor u2 = new Supervisor();
                u2.setId_Usuario((long) 2);
                u2.setNombre("Bender");
                u2.setApellidos("Rodriguez");
                u2.setContrasenia("asdf");
                u2.setDireccion("Calle Doblador");
                Date date2 = df.parse(String.valueOf("03121990"));
                u2.setFecha_nacimiento(date2);
                u2.setRol(Usuario.Rol.SUPERVISOR);
                u2.setTelefono("952111111");
                u2.setDni("222222222");

                Trabajador u3 = new Trabajador();
                u3.setId_Usuario((long) 3);
                u3.setNombre("Jon");
                u3.setApellidos("Nieve");
                u3.setContrasenia("asdf");
                u3.setDireccion("Calle Invernalia");
                Date date3 = df.parse(String.valueOf("01041989"));
                u3.setFecha_nacimiento(date3);
                u3.setRol(Usuario.Rol.TRABAJADOR);
                u3.setTelefono("952111111");
                u3.setDni("333333333");

                em.persist(u1);
                em.persist(u2);
                em.persist(u3);
            } catch (ParseException ex) {

            }
        }
    }

    @Override
    public Error compruebaLogin(Usuario u) {
        Usuario user = em.find(Usuario.class, u.getId_Usuario());
        if (user == null) {
            return Error.CUENTA_INEXISTENTE;
        }
        if (user.getContrasenia().equals(u.getContrasenia())) {
            return Error.NO_ERROR;
        } else {
            return Error.CONTRASENIA_INVALIDA;
        }

    }

    @Override
    public Usuario refrescarUsuario(Usuario u) {

        Error e = compruebaLogin(u);
        if (e != Error.NO_ERROR) {
            return null;
        }

        Usuario user = em.find(Usuario.class, u.getId_Usuario());
        em.refresh(user);
        return user;

    }
    
        @Override
    public void crearOrdenTrabajo(Usuario u, Aviso a){
        Orden o = new Orden();
        o.setAviso_O(a);
        o.setSupervisor_O((Supervisor) u);
        Random rnd = new Random();
        int lng = 0;
        do {
            lng = rnd.nextInt(1000000);
        } while (em.find(Orden.class, (long) lng) != null);
        o.setId_Orden((long)lng);
        o.setDescripcion("Descripción");
        o.setEstado(Orden.TiposEstado.abierto);
        
        Query q = em.createQuery("SELECT u FROM Usuario u");
        List<Usuario> l = q.getResultList();
        List<Trabajador> t = new ArrayList();
        for(Usuario user : l){
            if(user.getRol() == Usuario.Rol.TRABAJADOR){
                t.add((Trabajador)user);
            }
        }
        o.setTrabajadores(t);
        
        Supervisor s = (Supervisor)u;
        List<Orden> lo = s.getOrden();
        lo.add(o);
        s.setOrden(lo);
        
        List<Orden> loo = a.getOrdenes();
        loo.add(o);
        a.setOrdenes(loo);
        
        em.persist(o);

    }

    @Override
    public List<Aviso> getNAvisos() {
        Query query = em.createQuery("SELECT av FROM Aviso av");
        List<Aviso> lista = new ArrayList();
        List<Aviso> l = query.getResultList();
        Iterator i = l.iterator();
        while (i.hasNext()) {
            Aviso a = (Aviso) i.next();
            if (a.getEstado() == Aviso.TiposEstado.enCola) {
                lista.add(a);
            }
        }
        return lista;
    }

    @Override
    public List<Aviso> getLAvisos(Usuario u) {
        Supervisor s = (Supervisor) em.find(Usuario.class, u.getId_Usuario());
        List<Aviso> l = s.getAvisos();
        Iterator i = l.iterator();
        List<Aviso> lista = new ArrayList();
        while (i.hasNext()) {
            Aviso a = (Aviso) i.next();
            if (a.getEstado() == Aviso.TiposEstado.abierto || a.getEstado() == Aviso.TiposEstado.despachadoMovilidad || a.getEstado() == Aviso.TiposEstado.generadaOT) {
                lista.add(a);
            }

        }
        return lista;
    }

    @Override
    public List<Aviso> getCAvisos() {
        Query query = em.createQuery("SELECT av FROM Aviso av");
        List<Aviso> lista = new ArrayList();
        List<Aviso> l = query.getResultList();
        Iterator i = l.iterator();
        while (i.hasNext()) {
            Aviso a = (Aviso) i.next();
            if (a.getEstado() == Aviso.TiposEstado.cerrado) {
                lista.add(a);
            }
        }
        return lista;
    }

    @Override
    public List<Aviso> getAllAvisos() {
        Query query = em.createQuery("SELECT av FROM Aviso av");
        return (List<Aviso>) query.getResultList();
    }

    @Override
    public Afectado getAfectado(Long id) {
        Afectado af = em.find(Afectado.class, id);
        return af;
    }
    
    @Override
    public Aviso obtenerAviso(Long id) {
        Aviso av = em.find(Aviso.class, id);
        return av;
    }

    @Override
    public Aviso buscaAvisoOrden(Long l) {
        Orden o = em.find(Orden.class, l);
        return o.getAviso_O();
    }
    
    @Override
    public Afectado buscaAfectadoAviso(Long l) {
        Aviso av = em.find(Aviso.class, l);
        return av.getAfectados();
    }

    @Override
    public Aviso buscaAviso(Long a) {
        Aviso av = em.find(Aviso.class, a);
        return av;
    }

    @Override
    public List<Afectado> getAllAfectados() {
        Query query = em.createQuery("SELECT af FROM Afectado af");
        return (List<Afectado>) query.getResultList();
    }

    @Override
    public Error insertarNAA(Notificacion n, Usuario u, Aviso a, Afectado f) {
        Random rnd = new Random();
        int lng = 0;
        do {
            lng = rnd.nextInt(1000000);
        } while (em.find(Afectado.class, (long) lng) != null);
        f.setId_afectado((long) lng);

        do {
            lng = rnd.nextInt(1000000);
        } while (em.find(Aviso.class, (long) lng) != null);
        a.setId_Aviso((long) lng);
        a.setAfectados(f);
        a.setOperador_A((Operador) u);
        f.setAviso_A(a);

        do {
            lng = rnd.nextInt(1000000);
        } while (em.find(Notificacion.class, (long) lng) != null);
        n.setId_notificaciones((long) lng);
        n.setOperador_N((Operador) u);
        n.setAviso_N(a);
        a.setNotificaciones(n);

        Operador o = (Operador) em.find(Usuario.class, u.getId_Usuario());
        List<Notificacion> ln = o.getNotificaciones();
        List<Aviso> la = o.getAvisos();
        if (ln == null) {
            ln = new ArrayList();
        }
        if (la == null) {
            la = new ArrayList();
        }
        ln.add(n);
        la.add(a);
        o.setNotificaciones(ln);
        o.setAvisos(la);

        //Como se haria en cascada (La relaciones no son obligatorias
        em.persist(f);
        em.persist(a);
        em.persist(n);
        em.persist(o);

        return Error.NO_ERROR;
    }

    @Override
    public void actualizarAviso(Aviso a) {
        em.merge(a);
    }

    @Override
    public Aviso getAviso(Long id) {
        Aviso a = em.find(Aviso.class, id);
        return a;
    }

    @Override
    public List<Orden> getOrdenesTrabajador() {
        Query query = em.createQuery("SELECT ot FROM Orden ot");
        return (List<Orden>) query.getResultList();
    }

    @Override
    public Orden getOrdenTrabajador(Long id) {
        System.out.println("ESTO"+id);
        Orden o = em.find(Orden.class, id);
        return o;
    }
}
