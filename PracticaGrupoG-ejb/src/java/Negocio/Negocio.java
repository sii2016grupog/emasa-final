/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Entidades.Aviso;
import Entidades.Usuario;
import Entidades.Afectado;
import Entidades.Notificacion;
import Entidades.Orden;
import Entidades.Trabajador;
import java.util.List;
import javax.ejb.Local;

/**
 * @author GRUPO-G
 */
@Local
public interface Negocio {

    public static enum Error {
        CONTRASENIA_INVALIDA,
        CUENTA_INEXISTENTE,
        NO_ERROR
    };
    
    /* Crea los usuarios (uno de cada tipo) si 
     * no estan creados en la base de datos.*/
    public void crearUsuarios() ;
    
    /* Esta funcion comprueba que no hay errores
     * con el usuario que intenga loguearse.
     */
    public Error compruebaLogin(Usuario u);
    
    /* Devuelve el usuario u de la base de datos.*/
    public Usuario refrescarUsuario(Usuario u);
    
    /* Esta funcion crea una orden de trabajo con datos
     * iniciales y los relaciona con el aviso que lo
     * genera y con el usuario actual. */
    public void crearOrdenTrabajo(Usuario u, Aviso a);
    
    /* Obtiene una lista con todos los avisos cuyo estado
     * es "en cola" */
    public List<Aviso> getNAvisos();
    
    /* Obtiene una lista con todos los avisos cuyo estado 
     * no es "en cola" o "cerrado" */
    public List<Aviso> getLAvisos(Usuario u);

    /* Obtiene una lista con todos los avisos cuyo estado
     * es "cerrado".
     */
    public List<Aviso> getCAvisos();
    
    /* Obtiene una lista con todos los avisos */
    public List<Aviso> getAllAvisos();
    
    /* Obtiene un afectado dada su id */
    public Afectado getAfectado(Long id);
    
    /* Obtiene un aviso dada su id. */
    public Aviso obtenerAviso(Long id);
    
    /* Obtiene el aviso que ha generado la orden
     * con identificador id.
     */
    public Aviso buscaAvisoOrden(Long id);
    
    /* Obtiene el afectado de un aviso con 
     * identificador id.
     */
    public Afectado buscaAfectadoAviso(Long id);
    
    /* Al igual que obtener aviso, es una funcion 
     * que obtiene un aviso dada su id.
     */
    public Aviso buscaAviso(Long a);
    
    /* Inserta en la base de datos las notificaciones,
     * avisos y afectados rellenados por el operador.
     */
    public Error insertarNAA(Notificacion n, Usuario u, Aviso a, Afectado f);
    
    /* Actualiza en la base de datos (merge) el aviso a. */
    public void actualizarAviso(Aviso a);
    
    /* Devuelve una lista con todos los afectados
     * de la base de datos.
     */
    public List<Afectado> getAllAfectados();
    
    /* Al igual que las dos anteriores, obtiene un aviso a partir de su id.
     * No hemos combinado esto en una sola funcion porque hasta que 
     * empezamos a comentar este codigo no nos dimos cuenta y ya era
     * demasiado tarde para hacer cambios en el proyecto.
     */
    public Aviso getAviso(Long id);

    /* Devuelve una lista con todas las ordenes de la base de datos. */
    public List<Orden> getOrdenesTrabajador();
    
    /* Obtiene una orden a partir de su id. */
    public Orden getOrdenTrabajador(Long id);
    

}
