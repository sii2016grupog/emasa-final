/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Entidades.Afectado;
import Entidades.Aviso;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import Negocio.Negocio;
import java.util.ArrayList;

/**
 * @author GRUPO-G
 */
@Named(value = "controlAfectado")
@SessionScoped
public class ControlAfectado implements Serializable {

    @Inject
    private Negocio ap;
    @Inject
    private ControlAutorizacion ctrl;
    private List<Afectado> afectados;
    private Afectado afec=new Afectado();
    
    
    public List<Long> getAfectados() {
        List<Afectado> l= ap.getAllAfectados();
        List<Long> lid=new ArrayList();
        for(Afectado a:l){
            lid.add(a.getId_afectado());
        }
        return lid;
    }
    
    public Afectado getAfectado() {
        return afec;
    }

    public String setId(Long l) {
        afec = ap.buscaAfectadoAviso(l);
        return "afectado.xhtml";
    }

    public String Buscar(Long id) {
        afec = ap.getAfectado(id);
        return "afectados1.xhtml";
    }
    
    public String atras(){
        return "busqueda.xhtml";
    }

    public String logout() {
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        return "login.xhtml";
    }
}
