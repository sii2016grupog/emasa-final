/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Entidades.Aviso;
import Entidades.Orden;
import Entidades.Trabajador;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import Negocio.Negocio;



/**
 * @author GRUPO-G
 */
@Named(value = "controlOT")
@RequestScoped
public class ControlOT implements Serializable {
    
    @Inject
    private Negocio negocio;

    private Orden orden = new Orden();
    private List<Trabajador> lista_trab;
    private Trabajador trabajador;
    
    
    
    // Este metodo se llama al pulsar el boton modificar/ver orden. Prepara la orden que se va a presentar.
    public String setOrdenTrabajo(){
        // Como hago que se escoja el id correcto del formulario?
        System.out.println("p1" + orden.getId_Orden());
        orden = negocio.getOrdenTrabajador(orden.getId_Orden());
        lista_trab = orden.getTrabajadores();
        return "OT.xhtml";
    }
    
    public Orden getOrden() {
        return orden;
    }

    public Long get_Id(){
        return orden.getId_Orden();
    }
    public String get_Descripcion(){
        return orden.getDescripcion();
    }
    public List<Trabajador> getTrabajadores(){
        return orden.getTrabajadores();
    }
    public int getInitialDate(){
        return orden.getFecha_inicio();
    }
    public int getFinalDate(){
        return orden.getFecha_fin();
    }
    
    public String atras(){
        return "supervisor.xhtml";
    }
    
    public String atras1(){
        return "trabajador.xhtml";
    }
    
    public String atras2(){
        return "AvisoParticular.xhtml";
    }

    public String logout(){
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        orden = null;
        return "login.xhtml";
    }
}
