/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;


import Entidades.Aviso;
import Entidades.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import Negocio.Negocio;
import java.util.ArrayList;

/**
 * @author GRUPO-G
 */
@Named(value = "supervisorBean")
@SessionScoped
public class SupervisorBean implements Serializable {

    @Inject
    private Negocio ap;
    private Aviso nAviso = new Aviso(); 
    private Aviso lAviso = new Aviso();    
    private Aviso cAviso = new Aviso();   
    private Aviso tAviso = new Aviso();
    
    private List<Aviso> ListaTAviso = new ArrayList();;
    private List<Aviso> ListaNAviso = new ArrayList();
    private List<Aviso> ListaLAviso = new ArrayList();
    private List<Aviso> ListaCAviso = new ArrayList();

    
    
    
    public SupervisorBean() {
    }

    public Aviso getnAviso() {
        return nAviso;
    }

    public void setnAviso(Aviso nAviso) {
        this.nAviso = nAviso;
    }

    public void setlAviso(Aviso lAviso) {
        this.lAviso = lAviso;
    }

    public void setcAviso(Aviso cAviso) {
        this.cAviso = cAviso;
    }

    public void settAviso(Aviso tAviso) {
        this.tAviso = tAviso;
    }

    public Aviso getlAviso() {
        return lAviso;
    }

    public Aviso getcAviso() {
        return cAviso;
    }

    public Aviso gettAviso() {
        return tAviso;
    }
    
    public List<Aviso> getListaTavisos() {
        return ap.getAllAvisos();
    }
   
    public List<Aviso> getListaNavisos() {
        return ap.getNAvisos();
    }

    public List<Aviso> getListaLavisos(Usuario u) {
        return ap.getLAvisos(u);
    }

    public List<Aviso> getListaCavisos() {
        return ap.getCAvisos();
    }
    
    public void setListaTAviso(List<Aviso> ListaTAviso) {
        this.ListaTAviso = ListaTAviso;
    }

    
    public void setListaNAviso(List<Aviso> ListaNAviso) {
        this.ListaNAviso = ListaNAviso;
    }

    public void setListaLAviso(List<Aviso> ListaLAviso) {
        this.ListaLAviso = ListaLAviso;
    }

    public void setListaCAviso(List<Aviso> ListaCAviso) {
        this.ListaCAviso = ListaCAviso;
    }

    public String logout() {
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        return "login.xhtml";
    }
}
