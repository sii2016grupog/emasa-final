/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Entidades.Aviso;
import Entidades.Orden;
import Entidades.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import Negocio.Negocio;
import java.util.ArrayList;
/**
 *
 * @author Manu
 */

@Named(value = "controlBusqueda")
@SessionScoped
public class ControlBusqueda implements Serializable {
    
    @Inject
    private Negocio ap;
    @Inject
    private ControlAutorizacion ctrl;
    private List<Aviso> Avisos;
    private Aviso avis=new Aviso();
    private Orden o = new Orden();
    
    public List<Long> getAvisos() {
        List<Aviso> l= ap.getAllAvisos();
        List<Long> lid=new ArrayList();
        for(Aviso a:l){
            lid.add(a.getId_Aviso());
        }
        return lid;
    }
    
    public Aviso getAviso() {
        return avis;
    }
    
    public String nextPage(){
        return "busqueda.xhtml";
    }
    
    public String setId(Long l){
        avis=ap.buscaAvisoOrden(l);
        return "busqueda.xhtml";
    }
    
    public String Buscar(Long id){
        avis=ap.buscaAviso(id);
        return "busqueda.xhtml";
    }
    
    
    
}
