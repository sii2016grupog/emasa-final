package Vistas;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * @author GRUPO-G
 */
import Entidades.Afectado;
import Entidades.Aviso;
import Entidades.Notificacion;
import Entidades.Usuario;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.primefaces.event.FlowEvent;
import Negocio.Negocio;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;

@Named(value = "controlOperador")
@SessionScoped
public class ControlOperador implements Serializable {

    private Notificacion notificaciones = new Notificacion();
    private Aviso avisos = new Aviso();
    private Afectado afectados = new Afectado();

    @Inject
    private Negocio negocio;
    private ControlAutorizacion ctrl;

    public Notificacion getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(Notificacion notificaciones) {
        this.notificaciones = notificaciones;
    }

    public Aviso getAvisos() {
        return avisos;
    }

    public void setAvisos(Aviso avisos) {
        this.avisos = avisos;
    }

    public Afectado getAfectados() {
        return afectados;
    }

    public void setAfectados(Afectado afectados) {
        this.afectados = afectados;
    }

    public String onFlowProcess(FlowEvent event) {
        return event.getNewStep();
    }

    public String crear(Usuario u) {
        Negocio.Error e = negocio.insertarNAA(notificaciones, u, avisos, afectados);

        notificaciones.setId_notificaciones(null);
        notificaciones.setDescripcion(null);
        notificaciones.setFecha(null);
        notificaciones.setNotificador(null);
        notificaciones.setTlf_notificador(null);
        notificaciones.setEmail(null);

        avisos.setId_Aviso(null);
        avisos.setNombre(null);
        avisos.setGrado_Urgencia(null);
        avisos.setEstado(null);
        avisos.setInforme(null);

        afectados.setId_afectado(null);
        afectados.setNombre(null);
        afectados.setDireccion(null);
        afectados.setTelefono(null);
        afectados.setDisponibilidad(null);

        if (u.getRol() == Usuario.Rol.OPERADOR) { // Si el usuario es el administrador debe devolver la pÃ¡gina admin.xhtml
            return "operador.xhtml";
        }
        return "index.xhtml";
    }

    public void limpiarN() {
        notificaciones.setId_notificaciones(null);
        notificaciones.setDescripcion(null);
        notificaciones.setFecha(null);
        notificaciones.setNotificador(null);
        notificaciones.setTlf_notificador(null);
        notificaciones.setEmail(null);
    }

    public void limpiarAv() {
        avisos.setId_Aviso(null);
        avisos.setNombre(null);
        avisos.setGrado_Urgencia(null);
        avisos.setEstado(null);
        avisos.setInforme(null);
    }

    public void limpiarAf() {
        afectados.setId_afectado(null);
        afectados.setNombre(null);
        afectados.setDireccion(null);
        afectados.setTelefono(null);
        afectados.setDisponibilidad(null);
    }

    public String logout() {
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        return "login.xhtml";
    }

}
