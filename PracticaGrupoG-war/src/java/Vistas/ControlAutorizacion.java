/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Entidades.Usuario;
import Entidades.Usuario.Rol;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import Negocio.Negocio;

/**
 * @author GRUPO-G
 */
@Named(value = "controlAutorizacion")
@SessionScoped
public class ControlAutorizacion implements Serializable {

    @Inject
    private Negocio negocio;

    private Usuario usuario;

    /**
     * Creates a new instance of ControlAutorizacion
     */
    public ControlAutorizacion() {
    }

    public synchronized void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public synchronized Usuario getUsuario() {
        return usuario;
    }

    public String home() {
        // Implementar el mÃ©todo
        // Devuelve la pÃ¡gina Home dependiendo del rol del usuario
        if (usuario == null) {
            return "index.xhtml";
        } else if (usuario.getRol() == Rol.OPERADOR) { // Si el usuario es el administrador debe devolver la pÃ¡gina admin.xhtml
            return "operador.xhtml";
        } else if (usuario.getRol() == Rol.SUPERVISOR) { // Si el usuario es un usuario normal debe devolver la pÃ¡gina normal.xhtml
            return "supervisor.xhtml";
        } else if (usuario.getRol() == Rol.TRABAJADOR) { // Si el usuario es un usuario normal debe devolver la pÃ¡gina normal.xhtml
            return "trabajador.xhtml";
        } else { // Si no hay usuario debe devolver la pÃ¡gina de login
            return "index.xhtml";
        }
    }

    public synchronized void refrescarUsuario() {
        if (usuario != null) {
            usuario = negocio.refrescarUsuario(usuario);
        }
    }

    public String logout() {
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        usuario = null;
        return "index.xhtml";
    }

}
