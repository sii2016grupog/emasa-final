/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Entidades.Aviso;
import static Entidades.Aviso.TiposEstado.abierto;
import Entidades.Orden;
import Entidades.Supervisor;
import Entidades.Trabajador;
import Entidades.Usuario;
import javax.inject.Inject;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import java.io.Serializable;
import java.util.List;
import javax.faces.context.FacesContext;
import Negocio.Negocio;
import java.util.ArrayList;


/**
 * @author GRUPO-G
 */
@Named(value = "controlTrabajador")
@RequestScoped
public class ControlTrabajador implements Serializable {
    
    @Inject
    private Negocio negocio;

    private List<Orden> ordenes;
    
    private Trabajador trabajador;
    
    // Metodo que devuelve una lista con todas las ordenes de un trabajador.
    public List<Orden> getOrden(){
                   
        List<Orden> l= negocio.getOrdenesTrabajador();
       /*
       List<Orden> l = new ArrayList();
       List<Trabajador> lista_trab;
        lista_trab = new ArrayList<Trabajador>();
        lista_trab.add(new Trabajador(1,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C","952333333"));
        lista_trab.add(new Trabajador(7890,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C","952333333"));
        lista_trab.add(new Trabajador(7891,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C","952333333"));
        lista_trab.add(new Trabajador(7892,"trabajador", "asdf", "asdf", "calle falsa 789", "333333333C","952333333"));
        Aviso av = new Aviso();
        av.setId_Aviso((long)433324);
        Supervisor su = new Supervisor(123,"supervisor", "asdf", "asdf", "calle falsa 123", "111111111A","952111111");
        Orden o=new Orden();
        o.setAviso_O(av);
        o.setDescripcion("Una orden");
        o.setEstado(Orden.TiposEstado.abierto);
        o.setId_Orden((long)1);
        
        
        l.add(o);
        */
       
        List<Long> lid=new ArrayList();
        for(Orden a:l){
            lid.add(a.getId_Orden());
        }
        //return lid;
        return l;
    }

    public String logout(){
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        ordenes = null;
        return "login.xhtml";
    }
}
