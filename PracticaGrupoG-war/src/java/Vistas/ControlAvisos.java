/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Entidades.Aviso;
import Entidades.Orden;
import Entidades.Afectado;
import Entidades.Operador;
import Entidades.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import Negocio.Negocio;
import java.util.ArrayList;


/**
 * @author GRUPO-G
 */
@Named(value = "controlAviso")
@SessionScoped
public class ControlAvisos implements Serializable {

    @Inject
    private Negocio app;
    
    private Aviso aviso = new Aviso();
    private Afectado afec=new Afectado();
    private List<Orden> ordenes = new ArrayList();
    
    public String setAviso() {
        
        Long id = (long)0;
        aviso = app.getAviso(id);
        
        return "AvisoParticular.xhtml";
    }
    
    /*Prueba a partir de aquí*/
    
    private boolean editmode;   
    
    
    public String crearOT(Usuario u){
        app.crearOrdenTrabajo(u,aviso);
        return "supervisor.xhtml";
    }
    
    public void edit(){
        editmode = true;
    }
    
    public void save(){
        app.actualizarAviso(aviso);
        editmode = false;
    }
    
    public boolean isEditmode(){
        return editmode;
    }
    
    /*Termina prueba*/ 
    
    public Afectado getAfectado() {
        return afec;
    }
    
    public Entidades.Aviso.TiposEstado getEstado(){
        return aviso.getEstado();
    }
    
    public Entidades.Aviso.Urgencia getGrado_Urgencia(){
        return aviso.getGrado_Urgencia();
    }
    
    public List<Orden> getOrdenes(){
        ordenes = aviso.getOrdenes();
        return aviso.getOrdenes();
    }
    
    public String setAvisoN (long l){
        aviso = app.obtenerAviso(l);
        afec = aviso.getAfectados();
        return "AvisoParticular.xhtml";
    }
    
    public Operador getOperador_A(){
        return aviso.getOperador_A();
    }
    
    public Aviso getAviso(){
        return aviso;
    }

    public String logout(){
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        aviso = null;
        return "login.xhtml";
    }
}
