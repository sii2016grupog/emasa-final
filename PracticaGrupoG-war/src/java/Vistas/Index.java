/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

// cambiar al paquete donde esté el usuario y los roles
import Entidades.Usuario;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import Negocio.Negocio;

@Named(value = "login")
@RequestScoped

public class Index {
    // cambiar a la carpeta donde estén los roles

    @Inject
    private Negocio negocio;

    @Inject
    private ControlAutorizacion ctrl;

    private Usuario usuario;

    /**
     * Creates a new instance of Login
     */
    public Index() {
        usuario = new Usuario();
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public String autenticar() {
        negocio.crearUsuarios();
        Negocio.Error e = negocio.compruebaLogin(usuario);
        FacesMessage fm;

        switch (e) {
            case NO_ERROR:
                ctrl.setUsuario(negocio.refrescarUsuario(usuario));

                switch (ctrl.getUsuario().getRol()) {
                    case SUPERVISOR:

                        return "supervisor.xhtml";
                    case OPERADOR:
                        return "operador.xhtml";
                    case TRABAJADOR:
                        return "trabajador.xhtml";
                }
            case CUENTA_INEXISTENTE:
                fm = new FacesMessage("La cuenta no existe");
                FacesContext.getCurrentInstance().addMessage("login:user", fm);
                break;

            case CONTRASENIA_INVALIDA:
                fm = new FacesMessage("La contraseña no es correcta");
                FacesContext.getCurrentInstance().addMessage("login:pass", fm);
                break;
            default:
                fm = new FacesMessage("Error: " + e);
                FacesContext.getCurrentInstance().addMessage(null, fm);
                break;
        }
        return null;
    }
}
