/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Entidades.Aviso;
import Entidades.Orden;
import Entidades.Supervisor;
import Entidades.Afectado;
import Entidades.Notificacion;
import Entidades.Operador;
import Entidades.Trabajador;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.faces.context.FacesContext;


/**
 * @author GRUPO-G
 */
@Named(value = "controlCombinar")
@SessionScoped
public class ControlCombinar implements Serializable {

    private Aviso aviso, aviso1, aviso2;
    private List<Afectado> lista_afect;
    private List<Notificacion> lista_noti;
    private List<Orden> lista_orden;
    private List<Trabajador> lista_trab;
    private Operador operador_A ; 
    private Supervisor supervisor_su;
    
    public String setCombinar() {
       
        /*Supervisor no tendr�a por qu� estar asignado a ninguna orden, s�lo a sus avisos.*/
        return "CombinarAvisos.xhtml";
    }
    
    /*Prueba a partir de aquí*/
    
    private boolean editmode;
    
    public void edit(){
        editmode = true;
    }
    
    public void save(){
        /*entityService.save(entity);*/
        editmode = false;
    }
    
    public boolean isEditmode(){
        return editmode;
    }
    
    /*Termina prueba*/
    
    public void aniadirTrab(){
    }  
    
    public Afectado getAfectados(){
        return aviso.getAfectados();
    }
    
    public Entidades.Aviso.TiposEstado getEstado(){
        return aviso.getEstado();
    }
    
    public Entidades.Aviso.Urgencia getGrado_Urgencia(){
        return aviso.getGrado_Urgencia();
    }
    
    public List<Orden> getOrdenes(){
        return aviso.getOrdenes();
    }
    
    public Supervisor getSupervisor_su(){
        return supervisor_su;
    }
    
    public Operador getOperador_A(){
        return aviso.getOperador_A();
    }
    
    public Aviso getAviso(){
        return aviso;
    }
    
    public Aviso getAviso1(){
        return aviso1;
    }
    
    public Aviso getAviso2(){
        return aviso2;
    }

    public String logout(){
        // Destruye la sesiÃ³n (y con ello, el Ã¡mbito de este bean)
        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();
        aviso = null;
        return "login.xhtml";
    }
}
